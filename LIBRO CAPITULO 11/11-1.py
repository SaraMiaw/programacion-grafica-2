
#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
from math import log, ceil
def next_power_of_2(size):
    return 2 ** ceil(log(size, 2))
