import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *


def loadImage():
    img = pygame.image.load("textura2.jpg")
    textureData = pygame.image.tostring(img, "RGB", 1)
    width = img.get_width()
    height = img.get_height()
    bgImgGL = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, bgImgGL)#(a que se une la textura, nombre de la textura)
    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_LINEAR)
    glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, textureData)
    glEnable(GL_TEXTURE_2D)


def makeQuad():
    glPushMatrix()
    glScalef(0.5,0.5,0.5)
    glBegin(GL_QUADS)
    glTexCoord2f(0, 0)
    glVertex2f(25, 25)
    glTexCoord2f(0, 1)
    glVertex2f(25, 775)
    glTexCoord2f(1, 1)
    glVertex2f(775, 775)
    glTexCoord2f(1, 0)
    glVertex2f(775, 25)
    glEnd()
    glPopMatrix()


def main():
    pygame.init()
    display = (1280, 800)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)
    gluOrtho2D(0, 1280, 0, 800)
    loadImage()


    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        makeQuad()
        print(glGetIntegerv(GL_MAX_TEXTURE_SIZE))
        pygame.display.flip()


main()