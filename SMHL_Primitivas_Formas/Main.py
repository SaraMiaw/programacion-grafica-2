#Import OpenGL and GLU.  Don't import GLUT because it is ancient, broken, inflexible, and poorly
#designed--and we aren't using it.
from OpenGL.GL import *
from OpenGL.GLU import *
import string
#Import PyGame.  We'll mostly just use this to make a window.  Also import all the local
#declarations (e.g. pygame.KEYDOWN, etc.), so that we don't have to keep typing "pygame." in front
#of everything.  E.g., now we can do "KEYDOWN" instead of "pygame.KEYDOWN".
import pygame
from pygame.locals import *
import ICOSAEDRO as icosaedro
import CUBO as cubo
import PIRAMIDECUADRADA as piramideCuadrada
import PRISMA as prisma
import  ESFERA as esfera
import  CONO as cono
import OCTAHEDRO as octaedro
import CILINDRO as cilindro
import DODECAHEDRO as dodecahedro
from EJEMPLOSFUENTES import drawText
import LUCES
import sys



#Import some other useful modules
import sys, os, traceback
#Center the window on the screen, if we're on Windows, which supports it.
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
#Import sin, cos, radians, degrees, etc.
from math import *
#Initialize PyGame.  You could also call "pygame.init()", but in my experience this can be faster
#(since you aren't initializing *everything*) and more portable (since some modules may require
#extra dependencies).
#################PROBANDO COSAS################################################
# from tkinter import *
# raiz=Tk()
# raiz.title("Ventana de Pruebas")
# raiz.config(bg="pink")#bg es de back ground
# #BORDE Y CURSOR
# raiz.config(bd=25)
# raiz.config(relief="solid")
# raiz.config(cursor="coffee_mug")
# miFrame=Frame()
# #miFrame.pack(side="right") #se queda a la derecha
# #miFrame.pack(side="bottom")#parte inferior
# #miFrame.pack()
# miFrame.pack(side="left",anchor="n")#anchor son puntos cardinales nseo
# #miFrame.pack(fill="x")#redimencionar en x,y  both (todo),none(nada)
# #miFrame.pack(fill="y",expand=True)
# #########################
# #Color de FONDO
# miFrame.config(bg="coral")
# miFrame.config(width='650',height='350')
# #Al redimencionar la RAIZ nos daremos cuenta del tamano del FRAME
# ############################
# #BORDE
# miFrame.config(bd=45)#Borde(bd) con un grosor diferente a 0
# #miFrame.config(relief="groove")
# miFrame.config(relief="sunken")
# ############################
# #CURSOR
# miFrame.config(cursor="heart")
#################PROBANDO COSAS#################################################


pygame.display.init()
pygame.font.init()


#Screen configuration
#declaracion de la barible para cambiar de grafico
#font = pygame.font.SysFont("Times New Roman", 300)
texto='CUBO'
#text = font.render(texto, True, (255, 255, 255))
db=1
screen_size = [800,600]
# screen = pygame.display.set_mode((screen_size[0],screen_size[1]))



multisample = 0
#Set the window's icon, as applicable, to be just a transparent square.
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
#Set the title of the window.
pygame.display.set_caption("PyOpenGL Example - Ian Mallett - v.1.0.0 - 2013")
#Set the window to be multisampled.  This does depth testing at a higher resolution, leading to
#smooth, antialiased edges.  Most computers support at least multisample=4, and most support more
#(e.g. mine does 16).
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)
#Create the window of the requested size.  The pygame.OPENGL flag tells it to allow OpenGL to write
#directly to the window context.  The pygame.DOUBLEBUF flag tells it to make the window
#doublebuffered.  This causes the screen to only show a completed image.  This function actually
#returns a "surface" object, but it isn't useful for OpenGL programs.
pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

LUCES.IniciarPantalla(screen_size)
LUCES.IniciarIluminacion()
#If we draw a new pixel, we want to blend the new pixel with whatever is already there.  This allows
#for transparency, among other things.  Since everything here is fully opaque, we don't actually
#*need* this right now.
##glEnable(GL_BLEND)
##glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)

#Enable textured objects.  The glTexEnvi calls set up texturing in an intuitive way.  Again, since
#nothing here is textured, we don't actually *need* this right now.
##glEnable(GL_TEXTURE_2D)
##glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE)
##glTexEnvi(GL_POINT_SPRITE,GL_COORD_REPLACE,GL_TRUE)

#This requests that OpenGL make interpolation (filling in triangles) happen in the nicest way
#possible.  It's not guaranteed to happen; it's a request.
glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)
#This enables depth testing (so that closer objects are always drawn in front of farther objects).
#If depth testing is not enabled, then objects are drawn "over" each other in the order you draw
#them.  For most 3D rendering, you'll want depth testing enabled.
glEnable(GL_DEPTH_TEST)

#This concludes setup; the program itself will be a single triangle drawn in white at positions
#(0.0,0.0,0.0), (0.8,0.0,0.0), (0.0,0.0,0.4), along with a red, green, and blue line segments
#showing the axes.

#I find that an intuitive basic setup for the camera (where you're looking from) is to have the
#viewer located on the surface of a sphere surrounding everything.  You can change your position on
#the sphere, and thus fly around the scene.  To do this, I put the camera in (a kind of) spherical
#coordinates.

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0] #The sphere's center



#text =font.render("CUBO",True,(255,255,255))

def get_input():
    global camera_rot, camera_radius

    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()
    #Check how much the mouse moved since you last called this function.
    mouse_rel = pygame.mouse.get_rel()
    #List all the events that happened.
    for event in pygame.event.get():
        #Clicked the little "X"; close the window (return False breaks the main loop).
        if   event.type == QUIT: return False
        #If the user pressed a key:
        elif event.type == KEYDOWN:
            #If the user pressed the escape key, close the window.
            if   event.key == K_ESCAPE or event.key == pygame.K_RETURN: return False
        #If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            #Zoom in
            if   event.button == 4: camera_radius *= 0.9
            #Or out.
            elif event.button == 5: camera_radius /= 0.9


    #If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def draw():
    global db
    global texto


    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glViewport(0,0,screen_size[0],screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0])/float(screen_size[1]), 0.1,100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


    camera_pos = [
        camera_center[0] + camera_radius*cos(radians(camera_rot[0]))*cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius                            *sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius*sin(radians(camera_rot[0]))*cos(radians(camera_rot[1]))
    ]

    gluLookAt(
        camera_pos[0],camera_pos[1],camera_pos[2],
        camera_center[0],camera_center[1],camera_center[2],
        0,1,0
    )

    # cambiar de figura
    keypress = pygame.key.get_pressed()

    if keypress[pygame.K_1]:
        db = 1
        texto=("CUBO")

    if keypress[pygame.K_2]:
        db = 2
        texto = ("PIRAMIDE CUADRADA")

    if keypress[pygame.K_3]:
        db = 3
        texto = ("PRISMA ")

    if keypress[pygame.K_4]:
        db = 4
        texto = ("ESFERA")

    if keypress[pygame.K_5]:
        db = 5
        texto = ("CONO")
    if keypress[pygame.K_6]:
        db = 6
        texto = ("CILINDRO")
    if keypress[pygame.K_7]:
        db = 7
        texto = ("ICOSAEDRO")

    if keypress[pygame.K_8]:
        db = 8
        texto = ("OCTAHEDRO")

    if keypress[pygame.K_9]:
        db = 9
        texto = ("DODECAEDRO")

   #Dibujar Figuras

    if(db==1):
        cubo.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,0)
        drawText((-1, 0.5, 0), texto)
    if (db == 2):
        piramideCuadrada.draw(0,0,0,0.2,0.2,0.2,0,0,0,0.8,0.4,0.4)
        drawText((-10, 0.5, 0), texto)
    if (db == 3):
         prisma.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
         drawText((-1, 0.5, 0), texto)

    if (db == 4):
        esfera.draw(0,0,0,0.2,0.2,0.2,0,0,0,0,0,1)
        drawText((-1, 0.5, 0), texto)

    if (db == 5):
        cono.draw(0,0,0,0.2,0.2,0.2,90,180,90,0,1,1)
        drawText((-1, 0.5, 0), texto)

    if (db == 6):
        cilindro.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
        drawText((-1, 0.5, 0), texto)

    if (db == 7):
        icosaedro.draw(0,0,0,0.2,0.2,0.2,0,0,0,1,1,1)
        drawText((-1, 0.5, 0), texto)

    if (db == 8):
        octaedro.draw(0,0,0,0.2,0.2,0.2,0,0,0,0,0.5,1)
        drawText((-1, 0.5, 0), texto)

    if (db == 9):
        dodecahedro.draw(0,0,0,1,1,1,0,0,0,0.8,0.4,0.4)
        drawText((-1, 0.5, 0), texto)

    pygame.display.flip()


def main():

    clock = pygame.time.Clock()
    while True:
        if not get_input(): break

        draw()

        clock.tick(60) #Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()

if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()

