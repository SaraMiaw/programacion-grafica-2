from OpenGL.GL import *
from OpenGL.GLU import *



def draw(x,y,z,w,h,p,rx,ry,rz,r,g,b):
    glPushMatrix()
    glTranslatef(x,y,z)
    glRotatef(rx,1,0,0)
    glRotatef(ry, 0, 1, 0)
    glRotatef(rz, 0, 0, 1)
    glScalef(w,h,p)
    glColor3f(r,g,b)
    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glColor3f(0, 0, b)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glColor3f(r, g, b)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glEnd()


    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glColor3f(0, 0, b)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glColor3f(r, g, b)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()


    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glColor3f(0, 0, b)
    glVertex3f(-1.000000, 1.000000, 1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glColor3f(r, g, b)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glEnd()


    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glColor3f(0, 0, b)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glColor3f(r, g, b)
    glVertex3f(-1.000000, -1.000000, 1.000000)
    glEnd()


    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glColor3f(0, 0, b)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(1.000000, 1.000000, 1.000000)
    glColor3f(r, g, b)
    glVertex3f(1.000000, -1.000000, 1.000000)
    glEnd()


    glBegin(GL_QUADS)
    glColor3f(0, g, 0)
    glVertex3f(-1.000000, -1.000000, -1.000000)
    glColor3f(0, 0, b)
    glVertex3f(-1.000000, 1.000000, -1.000000)
    glColor3f(r, 0, 0)
    glVertex3f(1.000000, 1.000000, -1.000000)
    glColor3f(r, g, b)
    glVertex3f(1.000000, -1.000000, -1.000000)
    glEnd()
    glPopMatrix()

