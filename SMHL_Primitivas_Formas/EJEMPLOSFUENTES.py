
from OpenGL.GL import *
import pygame

def drawText(position, textString):
    font = pygame.font.SysFont ("Courier", 65)
    textSurface = font.render(textString, True, (255,0,255,255), (0,0,0,255))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(*position)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData.capitalize())