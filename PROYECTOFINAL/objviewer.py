#AUTORES:Ricardo Leon
#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

import sys, pygame
from pygame.locals import *
from pygame.constants import *
from OpenGL.GL import *
from OpenGL.GLU import *

# IMPORT OBJECT LOADER
from objloader import *
import BASE
import Sala
import Puerta
import CUBO as cubo
os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

info = pygame.display.Info() # You have to call this before pygame.display.set_mode()
screen_width,screen_height = info.current_w,info.current_h
viewport = (800,600)
hx = viewport[0]/2
hy = viewport[1]/2

window_width,window_height = screen_width-10,screen_height-50


srf = pygame.display.set_mode((window_width,window_height), OPENGL | DOUBLEBUF)


glLightfv(GL_LIGHT0, GL_POSITION,  (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)           # most obj files expect to be smooth-shaded
clock = pygame.time.Clock()

glMatrixMode(GL_PROJECTION)
glLoadIdentity()
width, height = viewport
gluPerspective(120.0, width/float(height), 1, 100000.0)
glEnable(GL_DEPTH_TEST)
glMatrixMode(GL_MODELVIEW)

# LOAD OBJECT AFTER PYGAME INIT
meshname = "bathroom.obj"
obj = OBJ(meshname, swapyz=True)



#rx, ry = (0,0)
rx, ry = (901,429)
#tx, ty = (0,0)
tx, ty = (-1474,-2630)
zpos = 77
rotate = move = False
while 1:
	clock.tick(30)
	for e in pygame.event.get():
		if e.type == QUIT:
			sys.exit()
		elif e.type == KEYDOWN and e.key == K_ESCAPE:
			sys.exit()
		elif e.type == MOUSEBUTTONDOWN:
			if e.button == 4: zpos = max(1, zpos-2)
			elif e.button == 5: zpos += 2
			elif e.button == 1: rotate = True
			elif e.button == 3: move = True
		elif e.type == MOUSEBUTTONUP:
			if e.button == 1: rotate = False
			elif e.button == 3: move = False
		elif e.type == MOUSEMOTION:
			i, j = e.rel
			if rotate:
				rx += i
				ry += j
				print("rotate: x=%d y=%d z=%d; " % (rx,ry,zpos))
			if move:
				tx += i*2
				ty -= j*2
				print("move: x=%d y=%d z=%d" % (tx,ty,zpos))

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glLoadIdentity()

	# RENDER OBJECT
	glTranslate(tx/20., ty/20., - zpos)
	glRotate(ry, 1, 0, 0)
	glRotate(rx, 0, 1, 0)
	glCallList(obj.gl_list)

	pygame.display.flip()