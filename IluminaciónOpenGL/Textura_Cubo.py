#Autora: Joana Estefanía Nicolalde
#Fecha:02/07/2020
#Curso: Programación Gráfica II
#Implementación de texturas en un Cubo
#Descripción: Se implementa las texturas en todas las caras de un cubo, tambien se utiliza la función Luces

#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

#Importamos todas las librerías que estamos utilizando
from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *
#Importamos la funcion de Luces creada
import LUCES
#Variables globales
global texid #id de textura
global varN #para la función de teclado
varN=1


if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"] = "1"
#Inicialización
pygame.display.init()
pygame.font.init()
#Tamaño de pantalla
screen_size = [800, 600]

icon = pygame.Surface((1, 1))
icon.set_alpha(0)
pygame.display.set_icon(icon)

pygame.display.set_caption("Triangles")

pygame.display.set_mode(screen_size, OPENGL | DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [30.0, 20.0]  # The spherical coordinates' angles (degrees).
camera_radius = 3.0  # The sphere's radius
camera_center = [0.0, 0.0, 0.0]  # The sphere's center

#Implementamos las funciones de LUCES
LUCES.IniciarPantalla([800,600])
LUCES.IniciarIluminacion()

def get_input():
    global camera_rot, camera_radius
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def setup_draw():
    # Clear the screen's color and depth buffers so we have a fresh space to draw geometry onto.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Setup the viewport (the area of the window to draw into)
    glViewport(0, 0, screen_size[0], screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )

#Funcion para dibujar
def draw():
    global texid #permite trabajar con la  variable global
    setup_draw()
    Teclado()   #Llamamos a las funciones del teclado
    #Creacion del primer cubo que sera base
    glPushMatrix()
    glColor(1,0,1)
    glScalef(10,0.1,5)
    glTranslate(0,-10,0)
    Cubo(3) #Implementa textura
    #Cubo2() #Solo mantiene color
    glPopMatrix()

    #Para cambiar de escena con el teclado
    if (varN==1):
        #Se puede dar color a las texturas
        glColor(0.7, 0.7, 1)
        loadTexture("textura_Ladrillo.jpg") #Metodo para crear texturas
        Cubo(1) #El parametro determina cuantas veces se va a repetir en una cara
        glPushMatrix()
        glColor(1,0.5,0.5)
        glTranslate(5, 0, 0)
        Cubo2()
        glPopMatrix()

    if(varN==2):
        glColor(1, 0.7, 1)
        textura2("sushitex.png")
        Cubo(3)
    if (varN == 3):
        glColor(0.7, 0.4, 1)
        loadTexture("rojo.jfif")
        Cubo(3)

    if (varN == 4):
        glColor(1, 0.4, 1)
        textura2("sushitex.png")
        Cubo(5)

    if (varN == 4):
        glColor(1, 0.4, 0.2)
        loadTexture("osito.jfif")
        Cubo(5)

    pygame.display.flip()



#Los diferentes vertices y aristas para el Cubo

#Cada una de las tuplas anteriores contiene dos números.
# Esos números corresponden a un vértice, y el "borde" se dibujará entre esos dos vértices.
# Comenzamos con 0, ya que así es como funcionan Python y la mayoría de los lenguajes de programación (el primer elemento es 0).
# Entonces, 0 corresponde al primer vértice que definimos (1, -1, -1) ... y así sucesivamente.

vertices = (
# x  y  z
(1, -1, -1),
(1, 1, -1),
(-1, 1, -1),
(-1, -1, -1),
(1, -1, 1),
(1, 1, 1),
(-1, -1, 1),
(-1, 1, 1)
)

edges = (
(0, 1),
(0, 3),
(0, 4),
(2, 1),
(2, 3),
(2, 7),
(6, 3),
(6, 4),
(6, 7),
(5, 1),
(5, 4),
(5, 7)
)


#Ahora creamos una funcion para llamar al cubo
def Cubo(v):
#para crear las caras
    glBegin(GL_QUADS)
    glTexCoord2f(0.0, 0.0) #Determina las coordenadas para las texturas, antes de cada vertices
    glVertex3f(-1.0, -1.0, 1.0)
    glTexCoord2f(v, 0.0)
    glVertex3f(1.0, -1.0, 1.0)
    glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, 1.0)
    glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, 1.0)


    glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(v, v)
    glVertex3f(-1.0, 1.0, -1.0)
    glTexCoord2f(0.0, v)
    glVertex3f(1.0, 1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, -1.0)


    glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, 1.0, 1.0)
    glTexCoord2f(v, 0.0)
    glVertex3f(1.0, 1.0, 1.0)
    glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, -1.0)


    #Coordenadas para la textura
    glTexCoord2f(v, v)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(0.0, v)
    glVertex3f(1.0, -1.0, -1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, 1.0)
    glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, 1.0)


    glTexCoord2f(v, 0.0)
    glVertex3f(1.0, -1.0, -1.0)
    glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, -1.0)
    glTexCoord2f(0.0, v)
    glVertex3f(1.0, 1.0, 1.0)
    glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, 1.0)


    glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, 1.0)
    glTexCoord2f(v, v)
    glVertex3f(-1.0, 1.0, 1.0)
    glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, -1.0)
    glEnd()

#En este cubo no se implementa texturas, por lo tanto no usa coordenadas de textura
def Cubo2():
#para crear las caras


    glBegin(GL_QUADS)
    #glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, -1.0, 1.0)
    #glTexCoord2f(v, 0.0)
    glVertex3f(1.0, -1.0, 1.0)
    #glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, 1.0)
    #glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, 1.0)


    #glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    #glTexCoord2f(v, v)
    glVertex3f(-1.0, 1.0, -1.0)
    #glTexCoord2f(0.0, v)
    glVertex3f(1.0, 1.0, -1.0)
    #glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, -1.0)


    #glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, -1.0)
    #glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, 1.0, 1.0)
    #glTexCoord2f(v, 0.0)
    glVertex3f(1.0, 1.0, 1.0)
    #glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, -1.0)



    #glTexCoord2f(v, v)
    glVertex3f(-1.0, -1.0, -1.0)
    #glTexCoord2f(0.0, v)
    glVertex3f(1.0, -1.0, -1.0)
    #glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, 1.0)
    #glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, 1.0)


    #glTexCoord2f(v, 0.0)
    glVertex3f(1.0, -1.0, -1.0)
    #glTexCoord2f(v, v)
    glVertex3f(1.0, 1.0, -1.0)
    #glTexCoord2f(0.0, v)
    glVertex3f(1.0, 1.0, 1.0)
    #glTexCoord2f(0.0, 0.0)
    glVertex3f(1.0, -1.0, 1.0)


    #glTexCoord2f(0.0, 0.0)
    glVertex3f(-1.0, -1.0, -1.0)
    #glTexCoord2f(v, 0.0)
    glVertex3f(-1.0, -1.0, 1.0)
    #glTexCoord2f(v, v)
    glVertex3f(-1.0, 1.0, 1.0)
    #glTexCoord2f(0.0, v)
    glVertex3f(-1.0, 1.0, -1.0)
    glEnd()



#   Funcion para crear texturas
def loadTexture(image):
    global texid
    #carga la imagen
    textureSurface = pygame.image.load(image)
    textureData = pygame.image.tostring(textureSurface, "RGBA", 1)
    #Determina el ancho y alto
    width = textureSurface.get_width()
    height = textureSurface.get_height()
    #Habilitamos la textura 2D
    glEnable(GL_TEXTURE_2D)
    texid = glGenTextures(1) #Genera el id para la textura

    glBindTexture(GL_TEXTURE_2D, texid)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData)
    #Los parametros que permiten la repeticion d elas caras
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    return texid

#Para esta textura solo muestra una parte d ela textura
def textura2(image):
    global texid
    # Load the textures
    textureSurface = pygame.image.load(image)
    # Retrieve the texture data
    texture_data = pygame.image.tostring(textureSurface, 'RGB', True)

    width = textureSurface.get_width()
    height = textureSurface.get_height()

    glEnable(GL_TEXTURE_2D)

    # Generate a texture id
    texid = glGenTextures(1)

    # Tell OpenGL we will be using this texture id for texture operations
    glBindTexture(GL_TEXTURE_2D, texid)

    gluBuild2DMipmaps(GL_TEXTURE_2D,
                      3,
                      width,
                      height,
                      GL_RGB,
                      GL_UNSIGNED_BYTE,
                      texture_data)


    # Tell OpenGL how to scale images
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)

    # Tell OpenGL that data is aligned to byte boundries
    glPixelStorei(GL_UNPACK_ALIGNMENT, 2)


    return texid

#Funcion del teclado
def Teclado():
    global  varN

    teclado = pygame.key.get_pressed()

    if teclado[K_1]:
        pygame.display.set_caption('Textura_normal_Cubo')
        varN = 1

    if teclado[K_2]:
        pygame.display.set_caption('Solo_una parte y lo demás ampliado')
        varN = 2

    if teclado[K_3]:
        pygame.display.set_caption('Repetición 3x3')
        varN = 3

    if teclado[K_4]:
        pygame.display.set_caption('Cubo')
        varN = 4

    if teclado[K_5]:
        pygame.display.set_caption('Icosaedro')
        varN = 5

    if teclado[K_6]:
        pygame.display.set_caption('Piramide')
        varN = 6

    if teclado[K_7]:
        pygame.display.set_caption('Octaedro')
        varN = 7

    if teclado[K_8]:
        pygame.display.set_caption('Esfera')
        varN = 8

    if teclado[K_9]:
        pygame.display.set_caption('Cono')
        varN = 9

def main():

    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        setup_draw()
        draw()

        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.


    pygame.quit()
#Permite eliminar las texturas para eliminar memoria
#glDeleteTextures(texid)

if __name__ == "__main__":
    try:
        main()

    except:

        traceback.print_exc()
        pygame.quit()

        input()
