
#Autor:Joana Estefanía Nicolalde
#Clase para implementar una textura
#Descripcion: Una clase para crear una textura con todos los parametros.Se la llama del Main

#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
class Textura:
    global texture_id
   #Inicializador
    def __init__(self, gl,glu,pygame):
        self.gl = gl
        self.glu = glu
        self.pygame=pygame
    #Funcion de creación de textura con parametro de imagen
    def crearTextura(self, imagen):
        # Load the textures
        global texture_id
        texture_surface = self.pygame.image.load(imagen)
        # Retrieve the texture data
        texture_data = self.pygame.image.tostring(texture_surface, 'RGB', True)

        # Generate a texture id
        texture_id = self.gl.glGenTextures(1)
        # Tell OpenGL we will be using this texture id for texture operations
        self.gl.glBindTexture(self.gl.GL_TEXTURE_2D, texture_id)

        # Tell OpenGL how to scale images
        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MAG_FILTER, self.gl.GL_LINEAR)
        self.gl.glTexParameteri(self.gl.GL_TEXTURE_2D, self.gl.GL_TEXTURE_MIN_FILTER, self.gl.GL_LINEAR)

        # Tell OpenGL that data is aligned to byte boundries
        self.gl.glPixelStorei(self.gl.GL_UNPACK_ALIGNMENT, 1)

        # Get the dimensions of the image
        width, height = texture_surface.get_rect().size

        # gluBuild2DMipmaps
        self.glu.gluBuild2DMipmaps(self.gl.GL_TEXTURE_2D,
                          3,
                          width,
                          height,
                          self.gl.GL_RGB,
                          self.gl.GL_UNSIGNED_BYTE,
                          texture_data)

