#Clase Esfera

#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
class Esfera:
    #Inicializador
    def __init__(self, gl,glu):
        self.gl = gl
        self.glu = glu
    #Funcion de creaciond e la esfera con sus diferentes vertices
    def crearEsfera(self,radio,p,q,r,g,b,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()
        #Transformaciones
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r,g,b)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_FILL);
        self.glu.gluSphere(sphere, radio,p,q );

        self.gl.glPopMatrix()

    #Creacion de la esfera en lineas (Wireframe)
    def crearEsferaWireFrame(self,radio,p,q,r,g,b,s,ex,ey,ez,tx,ty,tz,rx,ry,rz):
        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_LINE);
        self.glu.gluSphere(sphere, radio, p, q);

        self.gl.glPopMatrix()