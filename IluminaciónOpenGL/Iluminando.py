#Autor:Joana Estefania Nicolalde
#Fecha: 01/07/2020

#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
from OpenGL.GL import *
from OpenGL.GLU import *
import numpy as np
import pygame
import math
from pygame.locals import *
import sys, os, traceback
if sys.platform in ["win32","win64"]: os.environ["SDL_VIDEO_CENTERED"]="1"
from math import *
#Inicializamos
pygame.display.init()
pygame.font.init()
screen_size = [800,800]
multisample = 0
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
pygame.display.set_caption("PyOpenGL Example - Ian Mallett - v.1.0.0 - 2013")
if multisample:
    pygame.display.gl_set_attribute(GL_MULTISAMPLEBUFFERS,1)
    pygame.display.gl_set_attribute(GL_MULTISAMPLESAMPLES,multisample)

pygame.display.set_mode(screen_size,OPENGL|DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST)

glEnable(GL_DEPTH_TEST)

camera_rot = [30.0,20.0]      #The spherical coordinates' angles (degrees).
camera_radius = 3.0           #The sphere's radius
camera_center = [0.0,0.0,0.0]

#Luces
#habilita nuestra luz y que nuestra escena tenga iluminacion
glEnable(GL_LIGHT0);
glEnable(GL_LIGHTING);

#Luz ambiental
ambient= [1, 1, 1, 0.0]
#Luz difusa es la luz que proviene de una fuente puntual en particular (como el Sol) y
# golpea la superficie con una intensidad que depende de si la cara da hacia la luz o fuera de ella
#diffuse = [0.4, 0.4, 0.4, 0.0] #determina el color de la luz

#Luz especular, es lo que produce el brillo que destaca y nos ayuda a distinguir entre superficies planas
#specular = [1.0, 1.0, 1.0, 0.0]
position = [-25.0, 10.0, 10.0, 0.0] #Una posicion para la Luz

#glLightfv determina las caracteristicas de la luz
glLightfv(GL_LIGHT0, GL_POSITION, position,0);

#glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
#glLightfv(GL_LIGHT0, GL_DIFFUSE, [1,0,0,0]);

#glLightfv(GL_LIGHT0, GL_SPECULAR, specular,0);

#Atenuacion para la Luz
glLightfv(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.1)
glLightfv(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.05)



#sombras
glShadeModel(GL_SMOOTH);



#Material
#habilitamos el material
glEnable(GL_COLOR_MATERIAL);
#Definimos las luces para implementar con el material
luzAmbiente=[1,1,0.5,1]
luzDifusa=[1,1,1,10]
glMaterialfv(GL_BACK, GL_DIFFUSE, luzDifusa,0);
glMaterialfv(GL_FRONT, GL_AMBIENT, luzAmbiente,0);

#Los vertices y edges para el cubo
vertices= (
    (1, -1, -1),
    (1, 1, -1),
    (-1, 1, -1),
    (-1, -1, -1),
    (1, -1, 1),
    (1, 1, 1),
    (-1, -1, 1),
    (-1, 1, 1)
    )
#creamos los bordes
edges = (
    (0,1),
    (0,3),
    (0,4),
    (2,1),
    (2,3),
    (2,7),
    (6,3),
    (6,4),
    (6,7),
    (5,1),
    (5,4),
    (5,7)
    )

#creamos las caras
surfaces = (
(0,1,2,3),
(3,2,7,6),
(6,7,5,4),
(4,5,1,0),
(1,5,7,2),
(4,0,3,6)


)

#Colores para las diferentes caras
colors = (
(1,0,1),
(0,1,1),
(1,0,1),
(0,1,1),
(1,0.5,1),
(0,0,1),
(0.1,0,1),
(0,1,1),
(1,0.7,0),
(0,0,0.4),
(0.3,0,1),
(1,0,0.7),
)

#Cada una de las tuplas anteriores contiene dos números.
# Esos números corresponden a un vértice, y el "borde" se dibujará entre esos dos vértices.
# Comenzamos con 0, ya que así es como funcionan Python y la mayoría de los lenguajes de programación (el primer elemento es 0).
# Entonces, 0 corresponde al primer vértice que definimos (1, -1, -1) ... y así sucesivamente.

#Ahora creamos una funcion para llamar al cubo
def Cubo():
#para crear las caras
    glBegin(GL_QUADS)
    x=0
    for surface in surfaces:
        x+=1
        #coloca un solo color al cubo
        #glColor3fv((1,0.5,0.4))
        #coloca los colores en las diferentes caras
        glColor3fv(colors[x])
        for vertex in surface:
            glVertex3fv(vertices[vertex])
    glEnd()


    glBegin(GL_LINES)
    for edge in edges:
        for vertex in edge:
            glColor3fv((1, 1, 0.4))
            glVertex3fv(vertices[vertex])
    glEnd()


def get_input():
    global camera_rot, camera_radius


    keys_pressed = pygame.key.get_pressed()
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_position = pygame.mouse.get_pos()

    mouse_rel = pygame.mouse.get_rel()

    for event in pygame.event.get():

        if   event.type == QUIT: return False

        elif event.type == KEYDOWN:

            if   event.key == K_ESCAPE: return False

        elif event.type == MOUSEBUTTONDOWN:

            if   event.button == 4: camera_radius *= 0.9

            elif event.button == 5: camera_radius /= 0.9

    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True
#Funcion para dibujar
def draw(alto, radio):
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


    glViewport(0, 0, screen_size[0], screen_size[1])

    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()

    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]

    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )

    glColor3f(1, 1, 1)

    #Calcula las coordenadas de la base

    # glBegin(GL_POLYGON)
    #     # glColor3f(1, 0.50, 1)
    #     # for i in np.arange(0, 7, 0.1):
    #     #     auxX = math.sin(i) * radio
    #     #     auxZ = math.cos(i) * radio
    #     #     glVertex(auxX, 0, auxZ)
    #     #     glVertex(auxX, 0, auxZ)
    #     # glEnd()

    #Creamos el cono
    Cono(alto,radio)
    #Creacion del Suelo base, escalamos el cubo
    glPushMatrix()
    glTranslate(10,0,0)
    glScale(20,0.1,20)
    Cubo()
    glPopMatrix()

    #Cubo1- El cubo que se mostrara en la parte sup
    glPushMatrix()
    glTranslate(10,0,0)
    Cubo()
    glPopMatrix()


    #linea - Visualiza la posicion de la luz
    glBegin(GL_LINE_LOOP)
    glColor3f(1, 1, 1)

    glVertex(25.0, 10.0, 10.0)

    glVertex(20.0, 10.0, 10.0)
    glEnd()


    pygame.display.flip()


def Cono(alto,radio):
    glBegin(GL_QUAD_STRIP)
    for i in np.arange(0, 7, 0.1):
        auxX = math.sin(i) * radio
        glColor(0.1 * i, 0.2 * i, 0.1 * i)
        auxZ = math.cos(i) * radio
        glVertex(auxX, 0, auxZ)
        glVertex(0, alto, 0)
    glEnd()


def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        draw(1,0.5) #Llamamos a la funcion dibujar con los parametros para el cono

        clock.tick(60)
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()
