
#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

#importamos las bibliotecas
#GLU es una minibiblioteca de OpenGL
import pygame
import OpenGL
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math



#Ahora creamos una funcion para llamar al circulo
def Circulo():
#para crear las caras
    radio=0.8
    glBegin(GL_POLYGON)
    for i in range(0,1000):
        glColor3fv((0.5, 1, 0.4))
        a = (math.cos(i) * radio)
        b = (radio * math.sin(i))
        glVertex2f(a , b )
    glEnd()

def Cilindro():
    cylinder = gluNewQuadric();
    glColor3fv((1, 1, 0.4))
    gluQuadricDrawStyle(cylinder, GLU_FILL);
    gluCylinder(cylinder, 1, 1, 2, 25, 25);


#Ahora defin/mos el main
def main():
    pygame.init()
    display = (800,600) #damos la dimension de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0) #determina la perspectiva (campo de vision, relacion de aspecto, cerca,lejos)
    #Transformacion, retrocedemos cinco unidades para poder ver el circulo
    glTranslatef(0.0,0.0, -5)

#Bucle de eventos para pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(2, 2, 2, 2) #Transformacion de rotacion
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #limpiamos la pantalla
        #Circulo() #llamamos a cubo
        Cilindro()
        pygame.display.flip() #actualiza la pantalla
        pygame.time.wait(10)# espera de tiempo de 10 segundos
main()
