#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

#importamos las bibliotecas
#GLU es una minibiblioteca de OpenGL
import pygame
import OpenGL
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import math



#Ahora creamos una funcion para llamar al circulo
def Esfera2d():
#para crear las caras
    radio=1
    glBegin(GL_POLYGON)
    for i in range(0,1000):
        glColor3fv((1, 0.4, 0.4))
        a = 2*(math.cos(i) * radio)
        b = (radio * math.sin(i))
        glVertex2f(a , b )
    glEnd()


def Esfera():
    sphere= gluNewQuadric();
    glColor3fv((1, 1, 0.4))
    gluQuadricDrawStyle(sphere, GLU_FILL);
    gluSphere(sphere, 1, 20, 20);
    glColor3fv((0,0,0))
    gluQuadricDrawStyle(sphere, GLU_LINE);
    gluSphere(sphere, 1, 20, 20);

#Ahora defin/mos el main
def main():
    pygame.init()
    display = (800,600) #damos la dimension de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0) #determina la perspectiva (campo de vision, relacion de aspecto, cerca,lejos)
    #Transformacion, retrocedemos cinco unidades para poder ver el circulo
    glTranslatef(0.0,0.0, -5)

#Bucle de eventos para pygame
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT) #limpiamos la pantalla

          # Transformacion de rotacion
        #Esfera2d() #llamamos a la funcion
        glRotatef(2, 2, 2, 2)
        Esfera()
        pygame.display.flip() #actualiza la pantalla
        pygame.time.wait(10)# espera de tiempo de 10 segundos
main()
