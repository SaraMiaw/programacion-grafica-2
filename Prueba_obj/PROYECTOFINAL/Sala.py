from OpenGL.GL import *
import pygame
import CUBOT
import Cuadrotex


def drawmesa():

    CUBOT.drawtall(0,0,0,1,0.05,1,0,0,0,'./TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, -0.2, 0, 1, 0.05, 1, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPushMatrix()
    #PATAS DE LA MESA
    CUBOT.drawtall(-0.8, -0.2, 0.8, 0.08, 0.2, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.8, -0.2, 0.8, 0.08, 0.2, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.8, -0.2, -0.8, 0.08, 0.2, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.8, -0.2, -0.8, 0.08, 0.2, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPopMatrix()

def drawSillones(value):
    if value == 1:
        CUBOT.drawtall(0,-0.2,0, 2, 0.2, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(1, 0.2, 0, 1, 0.2, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(-1, 0.2, 0, 1, 0.2, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(-2, 0.2, 0.1, 0.2, 0.7, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(2, 0.2, 0.1, 0.2, 0.7, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(0, 0.3, -1, 2.2, 0.8, 0.2, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(-0.9, 0.9, -0.6, 0.9, 0.5, 0.2, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(0.9, 0.9, -0.6, 0.9, 0.5, 0.2, 0, 0, 0, './TEXTURAS/sillones.png')

    if value == 2:
        CUBOT.drawtall(0, -0.2, 0, 1, 0.2, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(0, 0.2, 0, 1, 0.2, 1, 0, 0, 0, './TEXTURAS/sillones.png')

        CUBOT.drawtall(-1, 0.2, 0.1, 0.2, 0.7, 1, 0, 0, 0, './TEXTURAS/sillones.png')
        CUBOT.drawtall(1, 0.2, 0.1, 0.2, 0.7, 1, 0, 0, 0, './TEXTURAS/sillones.png')

        CUBOT.drawtall(0, 0.29, -1.1, 1.1, 0.8, 0.2, 0, 0, 0, './TEXTURAS/sillones.png')

        CUBOT.drawtall(0, 0.9, -0.6, 0.7, 0.5, 0.2, 0, 0, 0, './TEXTURAS/sillones.png')

def drawmesapeq():

    CUBOT.drawtall(0,0.5,0,0.5,0.05,0.5,0,0,0,'./TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 0.3, 0, 0.5, 0.03, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, -0.35, 0, 0.5, 0.05, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPushMatrix()
    #PATAS DE LA MESA
    CUBOT.drawtall(-0.35, 0, 0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, 0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.35, 0, -0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, -0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPopMatrix()

def drawarmario():
    CUBOT.drawtall(0, 0, 0, 1, 0.5, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPushMatrix()
    glScalef(2, 1, 1)
    Cuadrotex.drawtall(-0.5, -0.5, 0.515, 0, 0, 0, './TEXTURAS/puertalibreria.png')
    glPopMatrix()
    CUBOT.drawtall(0, 2.5, 0, 1, 0.02, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 1.90, 0, 1, 0.02, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 1.25, 0, 1, 0.02, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 1, -0.5, 1, 1.5, 0.02, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-1, 1, 0, 0.02, 1.5, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(1, 1, 0, 0.02, 1.5, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')

def drawarmesatele():
#MESA
    CUBOT.drawtall(0, -0.4, 0, 1, 0.02, 0.5, 0, 0, 0, './TEXTURAS/mesatele.png')


    CUBOT.drawtall(0, 0.5, 0, 1, 0.02, 0.75, 0, 0, 0, './TEXTURAS/mesatele.png')
    CUBOT.drawtall(0, 0.1, 0, 1, 0.01, 0.5, 0, 0, 0, './TEXTURAS/mesatele.png')

    CUBOT.drawtall(0, 0, -0.5, 1, 0.5, 0.02, 0, 0, 0, './TEXTURAS/mesatele.png')

    CUBOT.drawtall(-1, 0, 0, 0.02, 0.5, 0.5, 0, 0, 0, './TEXTURAS/mesatele.png')
    CUBOT.drawtall(1, 0, 0, 0.02, 0.5, 0.5, 0, 0, 0, './TEXTURAS/mesatele.png')


def drawsilla():

    CUBOT.drawtall(0,0.5,0,0.5,0.05,0.5,0,0,0,'./TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 0.6, 0, 0.5, 0.05, 0.5, 0, 0, 0, './TEXTURAS/texturasilla.jpg')

    #ESPALDAR
    CUBOT.drawtall(0, 2, -0.35, 0.05, 0.5, 0.05, 0, 0, 90, './TEXTURAS/mesa2.png')

    CUBOT.drawtall(-0.45, 1.25, -0.35, 0.05, 0.75, 0.05, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.15, 1.25, -0.35, 0.04, 0.75, 0.04, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0, 1.25, -0.35, 0.04, 0.75, 0.04, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.15, 1.25, -0.35, 0.04, 0.75, 0.04, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.45, 1.25, -0.35, 0.05, 0.75, 0.05, 0, 0, 0, './TEXTURAS/mesa2.png')

    glPushMatrix()
    #PATAS DE LA MESA2
    CUBOT.drawtall(-0.35, 0, 0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, 0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.35, 0, -0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, -0.35, 0.08, 0.5, 0.08, 0, 0, 0, './TEXTURAS/mesa2.png')
    glPopMatrix()

def dibujarsala():
    glPushMatrix()

    glTranslatef(2.5, 0.3, 9.25)
    glRotatef(180,0,1,0)
    glScalef(0.5,0.5,0.5)
    drawSillones(1)


    glTranslatef(0,0, 4)
    glRotatef(180,0,1,0)
    glScalef(1.5,1.5,1)
    drawmesa()



    glTranslatef(-2.6,-0.1, 1)
    glRotatef(90,0,1,0)
    glScalef(1,0.5,0.5)
    drawSillones(2)



    glTranslatef(2.2,0, 0)
    drawmesapeq()

    glTranslatef(-4.5,0, 0)
    drawmesapeq()

    glTranslatef(6.3,0, -1)
    drawarmario()

    glTranslatef(0.5,0, 5)
    glRotatef(270, 0, 1, 0)
    drawarmesatele()

    glTranslatef(-2.5,0,0)
    drawsilla()
    glPopMatrix()