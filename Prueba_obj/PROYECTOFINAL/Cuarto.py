from OpenGL.GL import *
import pygame
import CUBOT
import Cuadrotex

def drawcama ():
    #Cobija
    CUBOT.drawtall(0.15,0,0,1.85,0.15,2,0,0,0,'./TEXTURAS/Textura_cama.jpg')
    CUBOT.drawtall(-2.1, 0, 0, 0.4, 0.15, 2, 0, 0, 0, './TEXTURAS/Textura_cama2.jpg')
    #Almohada
    CUBOT.drawtall(-2.1, 0.15, 0, 0.4, 0.15, 1, 0, 0, 0, './TEXTURAS/Textura_almohada.jpg')
    #Soporte
    CUBOT.drawtall(-0.25, -0.18, 0, 2.25, 0.025, 2, 0, 0, 0, './TEXTURAS/texcajo2.jpg')
    CUBOT.drawtall(2.1,-0.05,0,0.1,0.45,2,0,0,0,'./TEXTURAS/texcajo2.jpg')
    CUBOT.drawtall(-2.6, 0.15, 0, 0.1, 0.65, 2, 0, 0, 0, './TEXTURAS/texcajo2.jpg')

def drawescritorio ():
    CUBOT.drawtall(-0.25, -0.18, 0, 0.5, 0.15, 1.5, 0, 0, 0, './TEXTURAS/escritorio4.jpg')
    CUBOT.drawtall(-0.25, -0.68, -1, 0.5, 0.35, 0.5, 0, 0, 0, './TEXTURAS/escritorio4.jpg')
    CUBOT.drawtall(-0.25, -0.68, 1, 0.5, 0.35, 0.5, 0, 0, 0, './TEXTURAS/escritorio4.jpg')
    glPushMatrix()
    glScalef(1, 0.3, 1)
    Cuadrotex.drawtall(0.255, -3.05, -0.5, 0, 90, 0, './TEXTURAS/cajones.jpg')
    Cuadrotex.drawtall(0.255, -3.05, 1.5, 0, 90, 0, './TEXTURAS/cajones.jpg')
    Cuadrotex.drawtall(0.255, -2.05, -0.5, 0, 90, 0, './TEXTURAS/cajones.jpg')
    Cuadrotex.drawtall(0.255, -2.05, 1.5, 0, 90, 0, './TEXTURAS/cajones.jpg')
    glPopMatrix()
def drawcloset ():
    CUBOT.drawtall(0, 0, 0, 3.5, 2, 1, 0, 0, 0, './TEXTURAS/closet2.jpg')
    glPushMatrix()
    glScalef(2.3, 4, 1)
    Cuadrotex.drawtall(0, -0.5, 1.01, 0, 0, 0, './TEXTURAS/puerta_closet3m.jpg')
    Cuadrotex.drawtall(-1, -0.5, 1.01, 0, 0, 0, './TEXTURAS/puerta_closet3m.jpg')
    glPopMatrix()

#def velador ():

def drawcajonera ():
    CUBOT.drawtall(-0.25, -0.18, 0, 0.5, 0.5, 1.25, 0, 0, 0, './TEXTURAS/texcajo2.jpg')
    glPushMatrix()
    glScalef(1, 0.33, 2.5)
    #Textura_almohada.jpg
    #texcajo.jpg
    Cuadrotex.drawtall(0.251, -2.05, 0.5, 0, 90, 0, './TEXTURAS/cajon_cajonera.jpg')
    Cuadrotex.drawtall(0.251, -1.05, 0.5, 0, 90, 0, './TEXTURAS/cajon_cajonera.jpg')
    Cuadrotex.drawtall(0.251, -0.05, 0.5, 0, 90, 0, './TEXTURAS/cajon_cajonera.jpg')
    glPopMatrix()
def drawbanco ():
    CUBOT.drawtall(0, 0.5, 0, 0.5, 0.1, 0.5, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.35, 0, 0.35, 0.1, 0.5, 0.1, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, 0.35, 0.1, 0.5, 0.1, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(-0.35, 0, -0.35, 0.1, 0.5, 0.1, 0, 0, 0, './TEXTURAS/mesa2.png')
    CUBOT.drawtall(0.35, 0, -0.35, 0.1, 0.5, 0.1, 0, 0, 0, './TEXTURAS/mesa2.png')
