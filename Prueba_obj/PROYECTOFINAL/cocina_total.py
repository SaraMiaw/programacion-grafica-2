from OpenGL.GL import *
import CUBOT
import Cuadrotex


def drawMeson():
    CUBOT.drawtall(0, 0, 0, 2, 2, 0.1, 90, 0, 0, './TEXTURAS/marmol.jpg')
    glPushMatrix()
    glScalef(2, 1.2, 4)
    Cuadrotex.drawtall(-1, -1.04, -0.5, 90, 90, 0, './TEXTURAS/puerta_armario_manija.jpg')
    glPopMatrix()
    glPushMatrix()
    glScalef(2, 1.2, 4)
    Cuadrotex.drawtall(-1, -2.04, -0.5, 90, 90, 0, './TEXTURAS/puerta_armario_manija.jpg')
    glPopMatrix()
    glPushMatrix()
    glScalef(2, 1.2, 4)
    Cuadrotex.drawtall(-1, -3.04, -0.5, 90, 90, 0, './TEXTURAS/puerta_armario_manija.jpg')
    glPopMatrix()

def drawCocina():
    glPushMatrix()
    Cuadrotex.drawtall(-0.5, 0.5, -1, 90, 0, 0, './TEXTURAS/cocina_superior.jpg')
    glPopMatrix()
    glPushMatrix()
    Cuadrotex.drawtall(-0.5, -0.5, 0, 0, 0, 0, './TEXTURAS/cocina_frontal.jpg')
    glPopMatrix()

def drawNevera():
    CUBOT.drawtall(0, 0, 0, 1, 2, 1, 0, 0, 0, './TEXTURAS/nevera_textura.jpg')
    glPushMatrix()
    glScalef(2, 4, 1)
    Cuadrotex.drawtall(-0.5, -0.5, 1.01, 0, 0, 0, './TEXTURAS/nevera_frontal.jpg')
    glPopMatrix()


def drawArmario(value):
    if value ==1 :
        glPushMatrix()
        glScalef(2, 3, 1)
        Cuadrotex.drawtall(-0.5, -0.5, 0.51,0,0,0,'./TEXTURAS/puerta_armario_manija.jpg')
        glPopMatrix()
        glPushMatrix()
        glScalef(2, 2, 1)
        Cuadrotex.drawtall(-0.5, -0.75, -0.49,90,0,0,'./TEXTURAS/puerta_armario.jpg')
        glPopMatrix()
        glPushMatrix()
        glScalef(2, 3, 1)
        Cuadrotex.drawtall(0.5, -0.5, 0.51,0,90,0,'./TEXTURAS/puerta_armario.jpg')
        glPopMatrix()
    if value == 2:
        glPushMatrix()
        glScalef(2, 3, 1)
        Cuadrotex.drawtall(-0.5, -0.5, 0.51,0,0,0,'./TEXTURAS/puerta_armario_manija.jpg')
        glPopMatrix()
        glPushMatrix()
        glScalef(2, 2, 1)
        Cuadrotex.drawtall(-0.5, -0.75, -0.49,90,0,0,'./TEXTURAS/puerta_armario.jpg')
        glPopMatrix()
    if value ==3 :
        glPushMatrix()
        glScalef(2, 3, 1)
        Cuadrotex.drawtall(-0.5, -0.5, 0.51,0,0,0,'./TEXTURAS/puerta_armario_manija.jpg')
        glPopMatrix()
        glPushMatrix()
        glScalef(2, 2, 1)
        Cuadrotex.drawtall(-0.5, -0.75, -0.49,90,0,0,'./TEXTURAS/puerta_armario.jpg')
        glPopMatrix()
        glPushMatrix()
        glScalef(2, 3, 1)
        Cuadrotex.drawtall(-0.5, -0.5, 0.51,0,90,0,'./TEXTURAS/puerta_armario.jpg')
        glPopMatrix()

def draw():
    #PAREDES
    glPushMatrix()
    glScalef(10, 6, 1)
    Cuadrotex.drawtall(-0.1, -0.33, -1, 0, 0, 0, './TEXTURAS/cocina.jpg')
    glPopMatrix()

    glPushMatrix()
    glScalef(8, 6, 8)
    Cuadrotex.drawtall(1.125,-0.33, -0.125, 0, 90, 90, './TEXTURAS/cocina.jpg')
    glPopMatrix()

    #ARMARIOS
    #FILA1
    glPushMatrix()
    glTranslatef(2, 2.5, -0.455)
    drawArmario(3)
    glPopMatrix()
    glPushMatrix()
    glTranslatef(4, 2.5, -0.455)
    drawArmario(2)
    glPopMatrix()
    glPushMatrix()
    glTranslatef(6, 2.5, -0.455)
    drawArmario(2)
    glPopMatrix()
    glPushMatrix()
    glTranslatef(8, 2.5, -0.455)
    drawArmario(1)
    glPopMatrix()

    #FILA2
    glPushMatrix()
    glTranslatef(8.5, 2.5, 2)
    glRotatef(-90, 0, 1, 0)
    drawArmario(3)
    glPopMatrix()
    glPushMatrix()
    glTranslatef(8.5, 2.5, 4)
    glRotatef(-90, 0, 1, 0)
    drawArmario(2)
    glPopMatrix()
    glPushMatrix()
    glTranslatef(8.5, 2.5, 6)
    glRotatef(-90, 0, 1, 0)
    drawArmario(1)
    glPopMatrix()

    #NEVERA
    drawNevera()

    #COCINA
    glPushMatrix()
    glTranslatef(7, -1.04, 4)
    glRotatef(-90, 0, 1, 0)
    glScalef(2, 2, 2)
    drawCocina()
    glPopMatrix()

    #MESON DE TODA LA COCINA
    #FILA1
    glPushMatrix()
    glTranslatef(2, -0.19, 0)
    glRotatef(90, 0, 1, 0)
    glScalef(0.5, 0.5, 0.5)
    drawMeson()
    glPopMatrix()
    glPushMatrix()
    glTranslatef(4, -0.19, 0)
    glRotatef(90, 0, 1, 0)
    glScalef(0.5, 0.5, 0.5)
    drawMeson()
    glPopMatrix()
    glPushMatrix()
    glTranslatef(6, -0.19, 0)
    glRotatef(90, 0, 1, 0)
    glScalef(0.5, 0.5, 0.5)
    drawMeson()
    glPopMatrix()

    #FILA2
    glPushMatrix()
    glTranslatef(8, -0.19, 6)
    glScalef(0.5, 0.5, 0.5)
    drawMeson()
    glPopMatrix()
    glPushMatrix()
    glTranslatef(8, -0.19, 2)
    glScalef(0.5, 0.5, 0.5)
    drawMeson()
    glPopMatrix()