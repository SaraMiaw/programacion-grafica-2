import pygame
from pygame.locals import *
from pygame.constants import *
from OpenGL.GL import *
from OpenGL.GLU import *

# IMPORT OBJECT LOADER
from objloader import *
import BASE
import Sala
import Puerta
import CUBO as cubo
from math import *
import sys, os

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

info = pygame.display.Info() # You have to call this before pygame.display.set_mode()
screen_width,screen_height = info.current_w,info.current_h
viewport = (800,600)
hx = viewport[0]/2
hy = viewport[1]/2

window_width,window_height = screen_width-10,screen_height-50


srf = pygame.display.set_mode((window_width,window_height), OPENGL | DOUBLEBUF)


glLightfv(GL_LIGHT0, GL_POSITION,  (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)
glShadeModel(GL_SMOOTH)           # most obj files expect to be smooth-shaded
clock = pygame.time.Clock()
screen_size = [800,600]
glMatrixMode(GL_PROJECTION)
glLoadIdentity()
gluPerspective(70, float(screen_size[0]) / float(screen_size[1]), 0.1, 10000.0)
glMatrixMode(GL_MODELVIEW)
glLoadIdentity()
##############


# LOAD OBJECT AFTER PYGAME INIT
meshname = "deparobj.obj"
obj = OBJ(meshname, swapyz=True)



#rx, ry = (0,0)
rx, ry = (901,429)
tx, ty = (-1474,-2630)
posx,posy,posz=(0,0,0)
camx,camy,camz=(0,0,0)
zpos = 77
rotate = move = False
camara=0
mov=0.3

pitch=0
yaw=0
while 1:
	clock.tick(30)
	for e in pygame.event.get():
		if e.type == QUIT:
			sys.exit()
		elif e.type == KEYDOWN and e.key == K_ESCAPE:
			sys.exit()
		elif e.type == MOUSEBUTTONDOWN:
			if e.button == 4: zpos = max(1, zpos-2)
			elif e.button == 5: zpos += 2
			elif e.button == 1: rotate = True
			elif e.button == 3: move = True
		elif e.type == MOUSEBUTTONUP:
			if e.button == 1: rotate = False
			elif e.button == 3: move = False
		elif e.type == MOUSEMOTION:
			i, j = e.rel
			if rotate:
				rx += i
				ry += j
				print("rotate: x=%d y=%d z=%d; " % (rx,ry,zpos))
			if move:
				tx += i*2
				ty -= j*2
				print("move: x=%d y=%d z=%d" % (tx,ty,zpos))



	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
	glLoadIdentity()

	camera_rot = [30.0, 20.0]  # The spherical coordinates' angles (degrees).
	camera_radius = 3.0  # The sphere's radius
	camera_center = [0.0, 0.0, 0.0]  # The sphere's center

	camera_pos = [
		camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
		camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
		camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
	]
	keypress = pygame.key.get_pressed()
	mm = pygame.mouse.get_rel()

	if keypress[pygame.K_a]:
		posx = posx - mov

	if keypress[pygame.K_d]:
		posx = posx + mov

	if keypress[pygame.K_w]:
		posz = posz - mov

	if keypress[pygame.K_s]:
		posz = posz + mov


	if keypress[pygame.K_h]:
		camx = camx - mov

	if keypress[pygame.K_k]:
		camx = camx + mov

	if keypress[pygame.K_u]:
		camy = camy + mov

	if keypress[pygame.K_j]:
		camy = camy - mov

	if keypress[pygame.K_t]:
		camz = camz - mov

	if keypress[pygame.K_y]:
		camz = camz + mov

	if keypress[pygame.K_o]:
		camara = 0
	if keypress[pygame.K_i]:
		camara = 1
	if keypress[pygame.K_p]:
		camara = 2

	if keypress[pygame.K_4]:
		camara = 4

	if camara == 0:
		gluLookAt(
			camera_pos[0], camera_pos[1], camera_pos[2],
			camera_center[0], camera_center[1], camera_center[2],
			0, 1, 0
		)
	if camara == 1:
		gluLookAt(posx, 1, posz + 2, posx + 6.1416*cos(camx), 1 + camy, posz - 2+6.1416*cos(camz), 0, 1, 0)
	if camara == 2:
		gluLookAt(posx, 1, posz - 2, posx+camx, 1+camy, posz + 2+camz, 0, 1, 0)

	pitch += mm[1] * 0.1
	yaw += mm[0] * 0.1
	if camara == 4:
		gluLookAt(posx+3,1, posz+2, 5 * cos(radians(pitch)) * cos(radians(yaw)),
				  5 * sin(radians(pitch))+5, 5 * cos(radians(pitch)) * sin(radians(yaw)) + 5,
				  0, 1, 0)

	# RENDER OBJECT
	glTranslate(tx/20., ty/20., - zpos)
	glRotate(ry, 1, 0, 0)
	glRotate(rx, 0, 1, 0)
	glCallList(obj.gl_list)

	pygame.display.flip()