#MODIFICADO POR:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import pygame
from pygame.locals import *
import sys
from math import *
from Maths import invert
from OpenGLLibrary import *

pygame.init()

Screen = (800,600)
Window = glLibWindow(Screen,caption="Reflections!",multisample=True)
View = glLibView3D((0,0,Screen[0],Screen[1]),45)
View.set_view()

glLibTexturing(True)

ReflectionMapSize = 128
ReflectionMapView = glLibView3D((0,0,ReflectionMapSize,ReflectionMapSize),90)

Object = gluNewQuadric()
gluQuadricTexture(Object,True)

Camera = glLibCamera([0,0,7],[0,0,0])

import cubeobj
dlBox = cubeobj.get_list()

def DrawSphere(step,pos,size):
    glPushMatrix()
    glTranslatef(*pos)
    gluSphere(Object,size,step,step)
    glPopMatrix()

cubefaces = [GL_TEXTURE_CUBE_MAP_POSITIVE_X,
             GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
             GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
             GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
             GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
             GL_TEXTURE_CUBE_MAP_NEGATIVE_Z]

def InitGraphics():
    glEnable(GL_DEPTH_TEST)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)

    glClearColor(0.5,0.5,0.5,1.0)
def RenderToTextureData():
    x,y,width,height = glGetIntegerv(GL_VIEWPORT)
    glPixelStorei(GL_PACK_ALIGNMENT, 4)
    glPixelStorei(GL_PACK_ROW_LENGTH, 0)
    glPixelStorei(GL_PACK_SKIP_ROWS, 0)
    glPixelStorei(GL_PACK_SKIP_PIXELS, 0)
    data = glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE)
    if type(data) != type(""):
        data = data.tostring()
    return data
def UpdateCubeMaps(pos):
    x,y,z=pos
    ReflectionMapView.set_view()
    for i in xrange(0,6,1):
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        if   i == 0: gluLookAt(x,y,z, 1, 0, 0,0,-1,0)
        elif i == 1: gluLookAt(x,y,z,-1, 0, 0,0,-1,0)
        elif i == 2: gluLookAt(x,y,z, 0, 1, 0,0,0,1)
        elif i == 3: gluLookAt(x,y,z, 0,-1, 0,0,0,-1)
        elif i == 4: gluLookAt(x,y,z, 0, 0, 1,0,-1,0)
        elif i == 5: gluLookAt(x,y,z, 0, 0,-1,0,-1,0)
        DrawReflectees()
        glEnable(GL_TEXTURE_CUBE_MAP)
        glTexImage2D(cubefaces[i],0,GL_RGB,ReflectionMapSize,ReflectionMapSize,0,GL_RGB,GL_UNSIGNED_BYTE,RenderToTextureData())
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glDisable(GL_TEXTURE_CUBE_MAP)
def DrawReflectees():
    glCallList(dlBox)
ReflectorPos = [0,0,0]
def DrawReflectors():
    glEnable(GL_TEXTURE_CUBE_MAP)
    glEnable(GL_TEXTURE_GEN_S)
    glEnable(GL_TEXTURE_GEN_T)
    glEnable(GL_TEXTURE_GEN_R)
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP)
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP)
    glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_REFLECTION_MAP)

    view = glGetFloatv(GL_MODELVIEW_MATRIX)
    view = [[view[0][0],view[0][1],view[0][2]],
            [view[1][0],view[1][1],view[1][2]],
            [view[2][0],view[2][1],view[2][2]]]
    viewinv = invert(view)
    texmat = [[viewinv[0][0],viewinv[0][1],viewinv[0][2],0],
              [viewinv[1][0],viewinv[1][1],viewinv[1][2],0],
              [viewinv[2][0],viewinv[2][1],viewinv[2][2],0],
              [            0,            0,            0,1]]
    glMatrixMode(GL_TEXTURE)
    glPushMatrix()
    glMultMatrixf(texmat)
    glMatrixMode(GL_MODELVIEW)
    
    DrawSphere(40,ReflectorPos,0.75)
##    glPushMatrix()
##    glTranslatef(*ReflectorPos)
##    glutSolidTeapot(1.0)
##    glPopMatrix()

    glMatrixMode(GL_TEXTURE)
    glPopMatrix()
    glMatrixMode(GL_MODELVIEW)
    
    glDisable(GL_TEXTURE_GEN_S)
    glDisable(GL_TEXTURE_GEN_T)
    glDisable(GL_TEXTURE_GEN_R)
    glDisable(GL_TEXTURE_CUBE_MAP)
def Draw():
    UpdateCubeMaps(ReflectorPos)
    View.set_view()
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()
    Camera.set_camera()
    DrawReflectors()
    DrawReflectees()
    pygame.display.flip()
camerarot = [0,0]
def TakeScreenshot():
    data = RenderToTextureData()
##    surface = pygame.image.fromstring(data,(ReflectionMapSize,ReflectionMapSize),'RGB',1)
    surface = pygame.image.fromstring(data,(Screen[0],Screen[1]),'RGB',1)
    surface = pygame.transform.smoothscale(surface,(Screen[0]/2,Screen[1]/2))
    counter = 1
    while True:
        try:pygame.image.load(str(counter)+".png");counter+=1;continue
        except:pass
        pygame.image.save(surface,str(counter)+".png")
        break
radius = 7
def GetInput():
    global camerarot,radius
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            pygame.quit(); sys.exit()
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 4:
                radius -= 1
            if event.button == 5:
                radius += 1
        if event.type == KEYDOWN and event.key == K_s:
            TakeScreenshot()
##            pos = ReflectorPos
##            x,y,z=pos
##            ReflectionMapView.set_view()
##            for i in xrange(0,6,1):
##                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
##                glLoadIdentity()
##                if   i == 0: gluLookAt(x,y,z, 1, 0, 0,0,-1,0)
##                elif i == 1: gluLookAt(x,y,z,-1, 0, 0,0,-1,0)
##                elif i == 2: gluLookAt(x,y,z, 0, 1, 0,0,0,1)
##                elif i == 3: gluLookAt(x,y,z, 0,-1, 0,0,0,-1)
##                elif i == 4: gluLookAt(x,y,z, 0, 0, 1,0,-1,0)
##                elif i == 5: gluLookAt(x,y,z, 0, 0,-1,0,-1,0)
##                DrawReflectees()
##                TakeScreenshot()
    mpress = pygame.mouse.get_pressed()
    mrel = pygame.mouse.get_rel()
    if mpress[0]:
        camerarot[1] += mrel[0]
        camerarot[0] -= mrel[1]
    pos = [radius*cos(radians(camerarot[1]+90))*cos(radians(camerarot[0])),
           radius*sin(radians(camerarot[0])),
           radius*sin(radians(camerarot[1]+90))*cos(radians(camerarot[0]))]
    Camera.set_target_pos(pos)
def Update():
    Camera.update()
def main():
    InitGraphics()
    while True:
        GetInput()
        Update()
        Draw()
if __name__ == '__main__': main()
