#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *

if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"] = "1"

pygame.display.init()
pygame.font.init()

screen_size = [800, 600]

icon = pygame.Surface((1, 1))
icon.set_alpha(0)
pygame.display.set_icon(icon)

pygame.display.set_caption("Triangles")

pygame.display.set_mode(screen_size, OPENGL | DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [30.0, 20.0]  # The spherical coordinates' angles (degrees).
camera_radius = 3.0  # The sphere's radius
camera_center = [0.0, 0.0, 0.0]  # The sphere's center


def get_input():
    global camera_rot, camera_radius, st, sl
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
            if event.key == K_w:
                st += 1
            if event.key == K_s:
                st -= 1
            if event.key == K_a:
                sl -= 1
            if event.key == K_d:
                sl += 1
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def setup_draw():
    # Clear the screen's color and depth buffers so we have a fresh space to draw geometry onto.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Setup the viewport (the area of the window to draw into)
    glViewport(0, 0, screen_size[0], screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )


sl = 10
st = 10
def draw():
    setup_draw()
    sphere(sl, st)
    print(sl, " ", st)
    pygame.display.flip()


def sphere(sl, st):
    glColor3f(0.7, 0.4, 0.9)
    gluSphere(gluNewQuadric(), 2, sl, st)


def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        setup_draw()
        draw()
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()