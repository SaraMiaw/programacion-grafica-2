#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

#definimos el main
from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *


def Dodecaedro(r,g,b):

    glColor3f(r,g,b)
   # glScalef(0.005,0.005,0.005)


    glBegin(GL_POLYGON)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3


    glEnd()
    glColor3f(r+0.1, g+0.2, b-0.35)
    glBegin(GL_POLYGON)
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glEnd()

    glColor3f(r+0.5, g+0.5, b-0.5)
    glBegin(GL_POLYGON)
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glEnd()

    glColor3f(r-0.25, g+0.5, b-0.7)
    glBegin(GL_POLYGON)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glEnd()

    glColor3f(r+0.7, g-0.24, b+0.71)
    glBegin(GL_POLYGON)
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glVertex3f(- 13.499026,43.683765, - 41.545731)#10
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glColor3f(r+0.6, g, b)
    glBegin(GL_POLYGON)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(- 13.499026, 43.683765, 41.545731)  # 3
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glEnd()

    glColor3f(r, g+0.6, b)
    glBegin(GL_POLYGON)
    glVertex3f(- 35.340908, 26.998053, 25.676674)  # 12
    glVertex3f(- 43.683765, 43.683765, 0.000000)  # 13
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17

    glEnd()
    glColor3f(r+0.2300, g+0.5, b)
    glBegin(GL_POLYGON)
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(- 21.841883, 0.000000, - 15.869058)  # 18
    glVertex3f(- 35.340908, 26.998053, - 25.676674)  # 15
    glVertex3f(- 13.499026, 43.683765, - 41.545731)  # 10
    glEnd()

    glColor3f(r, g+0.5, b)
    glBegin(GL_POLYGON)
    glVertex3f(- 8.342857, 70.681816, 25.676674)  # 4
    glVertex3f(21.841883, 70.681816, 15.869058)  # 5
    glVertex3f(21.841883, 70.681816, - 15.869058)  # 8
    glVertex3f(- 8.342857, 70.681816, - 25.676674)  # 11
    glVertex3f(- 26.998053, 70.681816, 0.000000)  # 14
    glEnd()

    glColor3f(r, g, b)
    glBegin(GL_POLYGON)
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(13.499026, 26.998053, - 41.545731)  # 9
    glVertex3f(35.340908, 43.683765, - 25.676674)  # 7
    glEnd()

    glColor3f(r - 0.68, g + 0.21, b + 0.5)
    glBegin(GL_POLYGON)
    glVertex3f(13.499026, 26.998053, 41.545731)  # 1
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glVertex3f(43.683765, 26.998053, 0.000000)  # 6
    glVertex3f(35.340908, 43.683765, 25.676674)  # 2
    glEnd()

    glColor3f(r+0.8, g-0.5, b-0.5)
    glBegin(GL_POLYGON)
    glVertex3f(8.342857, 0.000000, 25.676674)  # 16
    glVertex3f(- 21.841883, 0.000000, 15.869058)  # 17
    glVertex3f(- 21.841883, 0.000000 ,- 15.869058)  # 18
    glVertex3f(8.342857, 0.000000, - 25.676674)  # 19
    glVertex3f(26.998053, 0.000000, - 0.000000)  # 20
    glEnd()





def main():
    pygame.init()
    display = (800,600) #resolucion de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

    glTranslatef(0.0,0.0, -5)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        glPushMatrix()
        glScalef(0.02, 0.02, 0.02)
        Dodecaedro(1,0,0.5)
        glPopMatrix()
        pygame.display.flip()
        pygame.time.wait(10)

#visual python
main()