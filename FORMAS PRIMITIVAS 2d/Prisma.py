#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

from OpenGL.GL import *
from OpenGL.GLU import *



def Prisma(r,g,b):

    glColor3f(r,g,b)
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,-1.0,0.0)
    glVertex3f (1.0,-1.0,-1.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (-1.0,-1.0,0.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,-1.0,1.0)
    glVertex3f (1.0,-1.0,-1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,-1.0,-1.0)
    glVertex3f (-1.0,-1.0,0.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (-1.0,-1.0,0.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,1.0,1.0)
    glVertex3f (1.0,-1.0,1.0)
    glEnd()
    glBegin(GL_TRIANGLES)
    glVertex3f (-1.0,1.0,0.0)
    glVertex3f (1.0,1.0,-1.0)
    glVertex3f (1.0,-1.0,-1.0)
    glEnd()

#definimos el main
def main():
    pygame.init()
    display = (800,600) #resolucion de la pantalla
    pygame.display.set_mode(display, DOUBLEBUF|OPENGL)

    gluPerspective(45, (display[0]/display[1]), 0.1, 50.0)

    glTranslatef(0.0,0.0, -5)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        glRotatef(1, 3, 1, 1)
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
        Prisma(1,0,0)
        pygame.display.flip()
        pygame.time.wait(10)

#visual python
main()