#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493

import pygame
from OpenGL.GL.shaders import GL_TRUE
from pygame.locals import *
from pygame.constants import *
from OpenGL.GL import *
from OpenGL.GLU import *

# IMPORT OBJECT LOADER
from objloader import *
import math
from math import *
import sys, os

#Centra la ventana a la pantalla
os.environ['SDL_VIDEO_CENTERED'] = '1'
#Inicializa todos los modulos de PyGame
pygame.init()

#La variable info guarda los atributos del alto y largo de la ventana
info = pygame.display.Info()  # Se debe llamar a esta funcion antes de pygame.display.set_mode()
#Se asigna el valor del largo y ancho de la ventana actual a nuevas variables
screen_width, screen_height = info.current_w, info.current_h
viewport = (800, 600)
hx = viewport[0] / 2
hy = viewport[1] / 2
#Carga la canción "Room of angel"
pygame.mixer_music.load("Room of angel.mp3")
#Se establece el tamaño de la ventana, para que esta ocupe un poco menos del tamaño de la pantalla
window_width, window_height = screen_width - 10, screen_height - 50
#Se inicializa una ventana para ser desplegada
srf = pygame.display.set_mode((window_width, window_height), OPENGL | DOUBLEBUF)

#Se crea una luz mediante la libreria OpenGL
glLightfv(GL_LIGHT0, GL_POSITION, (-40, 200, 100, 0.0))
glLightfv(GL_LIGHT0, GL_AMBIENT, (0.2, 0.2, 0.2, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.5, 0.5, 0.5, 1.0))
glEnable(GL_LIGHT0)
glEnable(GL_LIGHTING)
glEnable(GL_COLOR_MATERIAL)
#Se activa la profundidad de objetos
glEnable(GL_DEPTH_TEST)

#glDepthFunc(GL_LEQUAL)
glShadeModel(GL_SMOOTH)  # la mayoría de los archivos obj deben tener un sombreado suave
#Se crea un objeto que ayuda a controlar el paso del tiempo
clock = pygame.time.Clock()
screen_size = [800, 600]

# configuracion de movimiento

glMatrixMode(GL_PROJECTION)
#glOrtho(-100,100,-100,100,0.1,1000)
#gluPerspective(50, float(screen_size[0]) / float(screen_size[1]), 0.1, 1000.0)
glFrustum(-10,10,-10,10,20,1000)

glMatrixMode(GL_MODELVIEW)
gluLookAt(0, 2, -10, 0, 0, 0, 0, 0, 1)
viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)
glLoadIdentity()

#Se establece el centro de la ventana
displayCenter = [srf.get_size()[i] // 2 for i in range(2)]
#Se ubica al raton en el centro de la pantalla
mouseMove = [0, 0]
pygame.mouse.set_pos(displayCenter)

up_down_angle = 0.0
paused = False
run = True



#Se carga un obj despues de haber inicializado los modulos de PyGame
meshname = "./departamentoC4D/departamento.obj"
obj = OBJ(meshname, swapyz=True)

#Se inician variables auxiliares para controlar la posicion y rotacion del obj
rx, ry, rz = (-140, -40, 0)
tx, ty = (13, -3)
posx, posy, posz = (0, 0, 0)
camx, camy, camz = (0, 0, 0)
zpos = 77
rotate = move = False
camara = 0
movp = 1
movc = 0.03
pitch = 0
yaw = 0
#Se establece el volumen de la canción a reproducir
pygame.mixer.music.set_volume(0.4)
#Se inicializa la canción para su reproducción, el argumento "2" hace referencia al número de veces que se va a reproducir
pygame.mixer_music.play(2)

#Se establecen los eventos de teclado
while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_RETURN:
                run = False
            if event.key == pygame.K_PAUSE or event.key == pygame.K_p:
                paused = not paused
                pygame.mouse.set_pos(displayCenter)
        if not paused:
            if event.type == pygame.MOUSEMOTION:
                mouseMove = [event.pos[i] - displayCenter[i] for i in range(2)]
            pygame.mouse.set_pos(displayCenter)

    if not paused:
        # get para la tecla
        keypress = pygame.key.get_pressed()
        #mouseMove = pygame.mouse.get_rel()

        #Inicializa la matriz modelo vista
        glLoadIdentity()

        # Se aplica la mirada para arriba y abajo
        up_down_angle += mouseMove[1]*0.5
        glRotatef(up_down_angle, 1.0, 0.0, 0.0)

        #Se inicia la matriz vista
        glPushMatrix()
        glLoadIdentity()

        #Se aplica el movimiento
        if keypress[pygame.K_w]:
            glTranslatef(0,2,0)
        if keypress[pygame.K_s]:
            glTranslatef(0,-2,0)
        if keypress[pygame.K_d]:
            glTranslatef(-2,0,0)
        if keypress[pygame.K_a]:
            glTranslatef(2,0,0)


        if keypress[pygame.K_UP]:
            ry+=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_DOWN]:
            ry -= 1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_LEFT]:
            rx-=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_RIGHT]:
            rx+=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)

        if keypress[pygame.K_KP_PLUS]:
            ty+=1
        if keypress[pygame.K_KP_MINUS]:
            ty -= 1
        if keypress[pygame.K_KP_DIVIDE]:
            tx-=1
        if keypress[pygame.K_KP_MULTIPLY]:
            tx+=1


        #Aplica la rotación para la izquierda y la derecha
        glRotatef(-mouseMove[0]*0.5, 0.0, 0.0, 1.0)

        # Multiplica la matriz actual para obtener la nueva matriz de vista y almacenar la matriz de vista final
        glMultMatrixf(viewMatrix)
        viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)

        # Se aplica la matriz vista
        glPopMatrix()
        glMultMatrixf(viewMatrix)

        glLightfv(GL_LIGHT0, GL_POSITION, [1, -1, 1, 0])

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        glPushMatrix()



        glTranslatef(0,ry,rx)
        glRotatef(tx,1, 0, 0)
        glRotatef(ty, 0, 1, 0)
        glMaterialfv(GL_FRONT, GL_AMBIENT, 50.0)

        #Se llama a todos los poligonos dentro la lista del obj
        glCallList(obj.gl_list)

        glPopMatrix()
        # Actualiza toda la visualización de la superficie a la ventana desplegada
        pygame.display.flip()
        pygame.time.wait(10)
