#AUTORES:Sara Marina Haro Loor
#MAIL:smharol@uce.edu.ec
#ULTIMA MODIFICACION :23/9/20
#REFENRENCIA:http://www.pygame.org/wiki/OBJFileLoader/https://www.pygame.org/project/5134/7493
import pygame
from OpenGL.GL.shaders import GL_TRUE
from pygame.locals import *
from pygame.constants import *
from OpenGL.GL import *
from OpenGL.GLU import *

# IMPORT OBJECT LOADER
from objloader import *
import math
from math import *
import sys, os

os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

info = pygame.display.Info()  # You have to call this before pygame.display.set_mode()
screen_width, screen_height = info.current_w, info.current_h
viewport = (800, 600)
hx = viewport[0] / 2
hy = viewport[1] / 2
pygame.mixer_music.load("Room of angel.mp3")
window_width, window_height = screen_width - 10, screen_height - 50

srf = pygame.display.set_mode((window_width, window_height), OPENGL | DOUBLEBUF)
#Habilitar Luces

glEnable(GL_LIGHTING)
#glEnable(GL_LIGHT0)
glLightfv(GL_LIGHT0, GL_AMBIENT, (0, 0.0, 0.0, 1.0))
glLightfv(GL_LIGHT0, GL_DIFFUSE, (0.3059,0.0392,0.0392, 0.7))
glLightfv(GL_LIGHT0,GL_SPECULAR,(0.3059,0.0392,0.0392,1.0))


#glEnable(GL_LIGHT1)
glLightfv(GL_LIGHT1, GL_POSITION, (461, -37, 0, 1))
glLightfv(GL_LIGHT1, GL_DIFFUSE, (1,1,1, 0.3))
glLightfv(GL_LIGHT1,GL_SPECULAR,(1,1,1, 0.3))



#glEnable(GL_LIGHT1)

glEnable(GL_COLOR_MATERIAL)
glEnable(GL_DEPTH_TEST)

#glDepthFunc(GL_LEQUAL)
glShadeModel(GL_SMOOTH)  # most obj files expect to be smooth-shaded
clock = pygame.time.Clock()
screen_size = [800, 600]

# configuracion de movimiento


glMatrixMode(GL_PROJECTION)
#glOrtho(-100,100,-100,100,0.1,1000)
#gluPerspective(50, float(screen_size[0]) / float(screen_size[1]), 0.1, 1000.0)
glFrustum(-10,10,-10,10,20,1000)

glMatrixMode(GL_MODELVIEW)
gluLookAt(0, 2.5, -10, 0, 0, 0, 0, 0, 1)
viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)
glLoadIdentity()

displayCenter = [srf.get_size()[i] // 2 for i in range(2)]
mouseMove = [0, 0]
pygame.mouse.set_pos(displayCenter)

up_down_angle = 0.0
paused = False
run = True



# LOAD OBJECT AFTER PYGAME INIT
meshname = "./departamentoC4D/departamento.obj"
obj = OBJ(meshname, swapyz=True)

# rx, ry = (0,0)
rx, ry, rz = (-140, -40, 0)
tx, ty = (13, -3)
posx, posy, posz = (0, 0, 0)
camx, camy, camz = (0, 0, 0)
zpos = 77
rotate = move = False
camara = 0
movp = 1
movc = 0.05
pitch = 0
yaw = 0
pygame.mixer.music.set_volume(0.4)
pygame.mixer_music.play(2)

while run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_RETURN:
                run = False
            if event.key == pygame.K_PAUSE or event.key == pygame.K_p:
                paused = not paused
                pygame.mouse.set_pos(displayCenter)
        if not paused:
            if event.type == pygame.MOUSEMOTION:
                mouseMove = [event.pos[i] - displayCenter[i] for i in range(2)]
            pygame.mouse.set_pos(displayCenter)

    if not paused:
        # get keys
        keypress = pygame.key.get_pressed()
        #mouseMove = pygame.mouse.get_rel()

        # init model view matrix
        glLoadIdentity()

        # apply the look up and down
        up_down_angle += mouseMove[1]*0.5
        glRotatef(up_down_angle, 1.0, 0.0, 0.0)

        # init the view matrix
        glPushMatrix()
        glLoadIdentity()

        # apply the movment
        if keypress[pygame.K_w]:
            glTranslatef(0,2,0)
        if keypress[pygame.K_s]:
            glTranslatef(0,-2,0)
        if keypress[pygame.K_d]:
            glTranslatef(-2,0,0)
        if keypress[pygame.K_a]:
            glTranslatef(2,0,0)

        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, (0.3176,0.3529,0.3529,1))
        if keypress[pygame.K_UP]:
            glDisable(GL_LIGHT0)
            ry+=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_DOWN]:
            glEnable(GL_LIGHT0)
            ry -= 1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_LEFT]:
            glDisable(GL_LIGHT1)
            rx-=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)
        if keypress[pygame.K_RIGHT]:
            glEnable(GL_LIGHT1)
            rx+=1
            print("trasladado ry=", ry, " rx =", rx)
            print("rotado ty=", ty, " tx =", tx)

        if keypress[pygame.K_KP_PLUS]:
            ty+=1
        if keypress[pygame.K_KP_MINUS]:
            ty -= 1
        if keypress[pygame.K_KP_DIVIDE]:
            tx-=1
        if keypress[pygame.K_KP_MULTIPLY]:
            tx+=1


        # apply the left and right rotation
        glRotatef(-mouseMove[0]*0.5, 0.0, 0.0, 1.0)

        # multiply the current matrix by the get the new view matrix and store the final vie matrix
        glMultMatrixf(viewMatrix)
        viewMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)

        # apply view matrix
        glPopMatrix()
        glMultMatrixf(viewMatrix)

        glLightfv(GL_LIGHT0, GL_POSITION, [1, -1, 1, 0])

        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)

        glPushMatrix()



        glTranslatef(0,ry,rx)
        glRotatef(tx,1, 0, 0)
        glRotatef(ty, 0, 1, 0)
        glMaterialfv(GL_FRONT, GL_AMBIENT, 50.0)


        glCallList(obj.gl_list)

        glPopMatrix()

        pygame.display.flip()
        pygame.time.wait(10)
