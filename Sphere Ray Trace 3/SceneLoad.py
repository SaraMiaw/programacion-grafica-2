from Classes import *
from Maths import rndint, Rotate
def main():
    #Load Scene
    file = open("Scene.txt","r")
    Data = file.readlines()
    #Load Basic Scene Elements
    for line in Data:
        line = line.strip()
        if line.startswith("SIZE"):
            str = line[6:-1].split(",")
            RenderSize = (rndint(float(str[0])),rndint(float(str[1])))
        elif line.startswith("BACK"):
            str = line[5:]
            ViewBack = float(str)
        elif line.startswith("BGCL"):
            str = line[6:-1].split(",")
            BackgroundColor = (rndint(float(str[0])),rndint(float(str[1])),rndint(float(str[2])))
        elif line.startswith("LIGHTING"):
            if line.endswith("True"): Lighting = True
            elif line.endswith("False"): Lighting = False
        elif line.startswith("SHADOWS"):
            if line.endswith("True"): Shadows = True
            elif line.endswith("False"): Shadows = False
        elif line.startswith("REFRACTION"):
            if line.endswith("True"): Refraction = True
            elif line.endswith("False"): Refraction = False
        elif line.startswith("REFLECTION"):
            if line.endswith("True"): Reflection = True
            elif line.endswith("False"): Reflection = False
    #Load Lights
    LightsData = []
    for line in Data:
        if line.startswith("LIGHT "): LightsData.append(line[7:-2])
    Lights = []
    for lightstr in LightsData:
        str = lightstr.split(",")
        light = Light([float(str[0])+RenderSize[0]/2.0,float(str[1])+RenderSize[1]/2.0,float(str[2])],(255,255,255))
        Lights.append(light)
    #Load Objects
    GOLD = Material("Gold")
    SILVER = Material("Silver")
    MIRROR = Material("Mirror")
    Objects = []
    Rotations = []
    Scalar = 1.0
    for line in Data:
        line = line.strip()
        if line.startswith("SCALAR"):
            Scalar = float(line[7:])
        elif line.startswith("ROT"):
            RotData = line[4:]
            Angle = float(RotData[:-1])
            Axis = RotData[-1:]
            Rotations.append([Axis,Angle])
        elif line.startswith("SPHERE"):
            SphereData = line[8:].split(",")
            pos = [[float(SphereData[0])*Scalar,float(SphereData[1])*Scalar,float(SphereData[2][:-1])*Scalar]]
            for rotation in Rotations:
                pos = Rotate(pos,rotation[0],rotation[1])
            pos = pos[0]
            pos[0] += RenderSize[0]/2.0
            pos[1] += RenderSize[1]/2.0
            radius = float(SphereData[3])*Scalar
            mat = Material(SphereData[4])
            sphere = Sphere(pos,radius,mat)
            Objects.append(sphere)
    #Return Scene Data
    return [[RenderSize,ViewBack,BackgroundColor],Lights,Objects,[Lighting,Shadows,Reflection,Refraction]]
main()
