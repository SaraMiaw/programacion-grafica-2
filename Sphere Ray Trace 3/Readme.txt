Sphere Ray Tracer
Ian Mallett

-----------

Lately, I've realized that while PyOpenGL is fast and powerful, it may not be quite powerful enough to create the realism I want easily.

Ray-tracing is a way to create super-realistic lighting effects, among other things. In the real world, light is emitted from light sources, bounces around in the world, then to your eye.  This is essentially what Ray-tracing does, only it works backwards, computing only the rays that hit the camera.  Each ray can be traced to the point where it stopped reflecting, so it essentially supports infinite reflections.  Shadows, refraction and more are also feasible.  All this is impossible with PyOpenGL.

Anyway, this is more of a tech demo than anything--it only works with spheres, as they are mathematically simple.  Refraction is now supported, but it does have a few bugs.  I hope to fix that soon.

-----------

Here's how you make your own custom scenes!

The scene is loaded from Scene.txt.

I store my old scenes in Scenes/.  The current scene must be named "Scene.txt" and be in the current directory.

As the header at the top announces, you may use # to signify comments.

Each line represents a data object.  Unsuprisingly, you specify your scene with these.

"SIZE" tells the size the image will render at.  
"BACK" tells how far the eye is from the image.  Normally, this shouldn't be too small.
"BGCL" tells the background color.  For realism, I like to use (0,0,0) black, as there is no ambient lighting.

"LIGHT" defines a light at the given point.  There can be multiple lights, and adding them is as simple as adding another line.  There can be any number of lights, including 0.

"SCALAR" scales the scene.  Normally, you will want to develop your scene at a lowish resolution, then have it rendered at a higher one.  If your final render is two times the size of your working version, simply put SCALAR 2.  Scalar is not a mandatory call and is by default 1.

"ROT" rotates the scene.  This can be a bit tricky to understand, so you may have to play with it to figure it out.

"SPHERE" definees a sphere.  The tuple defines the sphere's position.  The number defines the sphere's radius.  The string at the end defines the material.  Valid materials are "Glass","Gold", "Mirror", or "Silver".

Make sure that after any of these lines, you do not leave spaces.

Leave the footer in.