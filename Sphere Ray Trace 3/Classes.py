from Maths import *
class Sphere:
    def __init__(self,pos,size,mat):
        self.pos = pos
        self.size = size
        self.mat = mat
class Light:
    def __init__(self,pos,color):
        self.pos = pos
        self.color = color
class Ray:
    def __init__(self,pos,dir,index=1.0):
        self.pos = pos
        self.dir = Normalize(dir)
        self.index = float(index)
class Material:
    def __init__(self,type):
        if type == "Gold":
            self.color = (240,198,0)
            self.reflect = 0.5
            self.trans = 1.0
        if type == "Silver":
            self.color = (193,195,190)
            self.reflect = 0.7
            self.trans = 1.0
        if type == "Mirror":
            self.color = (50,50,50)
            self.reflect = 0.95
            self.trans = 1.0
        if type == "Glass":
            self.color = (255,255,255)
            self.reflect = 0.95
            self.trans = 0.05
            self.refract = 1.52
