import pygame
from pygame.locals import *
import sys, os
import Render
if sys.platform == 'win32' or sys.platform == 'win64':
    os.environ['SDL_VIDEO_CENTERED'] = '1'
pygame.init()

RenderSurface = Render.main()

Screen = RenderSurface.get_size()

pygame.display.set_caption("Sphere Ray-Tracer - v.5.0.0 - Ian Mallett - 2008")
icon = pygame.Surface((1,1)); icon.set_alpha(0); pygame.display.set_icon(icon)
Surface = pygame.display.set_mode(Screen)

def GetInput():
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit(); sys.exit()
def Draw():
    Surface.blit(RenderSurface,(0,0))
    pygame.display.flip()
def main():
    while True:
        GetInput()
        Draw()
if __name__ == '__main__': main()
